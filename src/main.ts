import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import swaggerConfig from './config/swagger.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const appPort: number = parseInt(process.env.APP_PORT) || 3000;
  app.setGlobalPrefix('api');

  swaggerConfig(app);

  await app.listen(appPort);

  const logger = new Logger(bootstrap.name);
  logger.log(`App is listening on port ${appPort}`);
}

bootstrap();
