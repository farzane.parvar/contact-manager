import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contact } from './contact.entity';
import { ContactRepository } from './contact.repository';
import { CreateContactDto } from './dto/create-contact.dto';
import { FilterContactDto } from './dto/filter-contact.dto';
import { UpdateContactDto } from './dto/update-contact.dto';

@Injectable()
export class ContactService {
  constructor(
    @InjectRepository(ContactRepository)
    private contactRepository: ContactRepository,
  ) {}

  async filter(fitlerContactDto: FilterContactDto): Promise<Contact[]> {
    const { name, phone_number } = fitlerContactDto;

    return this.contactRepository.filterContacts({
      name: name?.toLowerCase(),
      phone_number,
    });
  }

  async getById(id: number): Promise<Contact> {
    const contact = await this.contactRepository.getContactById(id);

    if (!contact) {
      throw new NotFoundException(`Contact with ID ${id} is not found.`);
    }

    return contact;
  }

  async create(createContactDto: CreateContactDto): Promise<Contact> {
    const { phone_number } = createContactDto;

    // Fetch contact with the provided phone number
    const existContact = await this.contactRepository.filterContacts({
      phone_number,
      name: '',
    });

    // Throw an exception if the phone number is already registered for another contact
    if (existContact.length) {
      throw new BadRequestException(
        `Phone number ${phone_number} is already exist for ${existContact[0].fname} ${existContact[0].lname}`,
      );
    }

    return this.contactRepository.createContact(createContactDto);
  }

  async update(
    id: number,
    updateContactDto: UpdateContactDto,
  ): Promise<Contact> {
    const contact = await this.contactRepository.getContactById(id);

    // Throw an error if the asked contact doesn't exist
    if (!contact) {
      throw new NotFoundException(`Contact with ID ${id} is not found.`);
    }

    const { phone_number } = updateContactDto;

    // Fetch another contact with the provided phone number
    if (phone_number) {
      const existContact = await this.contactRepository
        .filterContacts({
          phone_number,
          name: '',
        })
        .then((contacts) => contacts.filter((contact) => contact.id !== id));

      // Throw an exception if there's another contact with the given phone number
      if (existContact.length) {
        throw new BadRequestException(
          `Phone number ${phone_number} is already exist for ${existContact[0].fname} ${existContact[0].lname}`,
        );
      }
    }

    return this.contactRepository.updateContact(contact, updateContactDto);
  }

  async delete(id: number): Promise<number> {
    const contact = await this.contactRepository.getContactById(id);

    // Throw an error if the asked contact doesn't exist
    if (!contact) {
      throw new NotFoundException(`Contact with ID ${id} is not found.`);
    }

    await this.contactRepository.deleteContact(contact);
    return contact.id;
  }
}
