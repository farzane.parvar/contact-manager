import { EntityRepository, Repository } from 'typeorm';
import { Contact } from './contact.entity';
import { CreateContactDto } from './dto/create-contact.dto';
import { FilterContactDto } from './dto/filter-contact.dto';
import { UpdateContactDto } from './dto/update-contact.dto';

@EntityRepository(Contact)
export class ContactRepository extends Repository<Contact> {
  filterContacts(fitlerContactDto: FilterContactDto): Promise<Contact[]> {
    const { name, phone_number } = fitlerContactDto;
    const query = this.createQueryBuilder('contacts');

    // Filter contacts by their phone number
    if (phone_number) {
      query.andWhere('contacts.phone_number LIKE :phone_number', {
        phone_number: `%${phone_number}%`,
      });
    }

    // Filter contacts by their first and last name
    if (name) {
      query.andWhere(
        'LOWER(contacts.fname) LIKE :name OR LOWER(contacts.lname) LIKE :name',
        {
          name: `%${name}%`,
        },
      );
    }

    return query.getMany();
  }

  getContactById(id: number): Promise<Contact> {
    return this.createQueryBuilder('contacts')
      .where('contacts.id = :id', { id })
      .andWhere('contacts.deleted_at IS NULL')
      .getOne();
  }

  async createContact(createContactDto: CreateContactDto): Promise<Contact> {
    const { fname, lname, phone_number } = createContactDto;
    const contact = new Contact();
    contact.fname = fname;
    contact.lname = lname;
    contact.phone_number = phone_number;
    return contact.save();
  }

  async updateContact(
    contact: Contact,
    updateContactDto: UpdateContactDto,
  ): Promise<Contact> {
    const { fname, lname, phone_number } = updateContactDto;
    contact.fname = fname || contact.fname;
    contact.lname = lname || contact.lname;
    contact.phone_number = phone_number || contact.phone_number;
    contact.updated_at = new Date();
    await contact.save();
    return contact;
  }

  // Soft delete contact
  async deleteContact(contact: Contact): Promise<Contact> {
    contact.deleted_at = new Date();
    return contact.save();
  }
}
