import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Response } from 'express';
import { ApiBadRequestResponse, ApiResponse } from '@nestjs/swagger';
import { Contact } from './contact.entity';
import { ContactService } from './contact.service';
import { CreateContactDto } from './dto/create-contact.dto';
import { FilterContactDto } from './dto/filter-contact.dto';
import { UpdateContactDto } from './dto/update-contact.dto';

@Controller('contacts')
export class ContactController {
  constructor(private contactServie: ContactService) {}

  /**
   * Filter contacts by their first name, last name and phone number
   * If the user hasn't given anything, it just returns all contact entities
   */
  @ApiResponse({
    status: 200,
    description: 'Find all matched contacts',
  })
  @ApiBadRequestResponse({
    description: "Validation's failed",
  })
  @Get('')
  async filter(
    @Res() res: Response,
    @Query(ValidationPipe) fitlerContactDto: FilterContactDto,
  ) {
    const contacts = await this.contactServie.filter(fitlerContactDto);

    res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      payload: {
        contacts,
      },
      metadata: {
        count: contacts.length,
      },
    });
  }

  /**
   * Return a contact by its id
   */
  @ApiResponse({
    status: 200,
    description: 'Get a contact by its id',
  })
  @ApiBadRequestResponse({
    description: "Validation's failed / contact doesn't exist",
  })
  @Get('/:id')
  async getById(@Res() res: Response, @Param('id', ParseIntPipe) id: number) {
    const contact = await this.contactServie.getById(id);

    res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      payload: {
        contact,
      },
    });
  }

  /**
   * Create a new contact with given information
   */
  @ApiResponse({
    status: 201,
    description: 'Create a contact',
  })
  @ApiBadRequestResponse({
    description: "Validation's failed / phone number is already exist",
  })
  @Post()
  @UsePipes(ValidationPipe)
  async create(
    @Res() res: Response,
    @Body() createContactDto: CreateContactDto,
  ) {
    const contact = await this.contactServie.create(createContactDto);

    res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.CREATED,
      payload: {
        contact,
      },
    });
  }

  /**
   * Update a contact by its id
   */
  @ApiResponse({
    status: 200,
    description: 'Update a contact by its id',
  })
  @ApiBadRequestResponse({
    description:
      "Validation's failed / contact doesn't exist / phone number is already exist for another contact",
  })
  @Put('/:id')
  @UsePipes(ValidationPipe)
  async update(
    @Res() res: Response,
    @Param('id', ParseIntPipe) id: number,
    @Body() updateContactDto: UpdateContactDto,
  ) {
    const contact = await this.contactServie.update(id, updateContactDto);

    res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      payload: {
        contact,
      },
    });
  }

  /**
   * Soft deleted a contact
   */
  @ApiResponse({
    status: 200,
    description: 'Sofe delete a contact by its id',
  })
  @ApiBadRequestResponse({
    description: "Validation's failed / contact doesn't exist",
  })
  @Delete('/:id')
  async delete(@Res() res: Response, @Param('id', ParseIntPipe) id: number) {
    const contactId = await this.contactServie.delete(id);

    res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      payload: {
        deleted_id: contactId,
      },
    });
  }
}
