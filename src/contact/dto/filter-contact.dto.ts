import { ApiProperty } from '@nestjs/swagger';
import {
  IsAlpha,
  IsNumberString,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';

export class FilterContactDto {
  @ApiProperty()
  @IsString()
  @IsOptional()
  @IsAlpha()
  name: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  @Length(1, 15)
  @IsNumberString({
    no_symbols: true,
  })
  phone_number: string;
}
