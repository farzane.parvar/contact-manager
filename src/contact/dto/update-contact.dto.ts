import { ApiProperty } from '@nestjs/swagger';
import {
  IsAlpha,
  IsNumberString,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';

export class UpdateContactDto {
  @IsString()
  @IsOptional()
  @Length(2, 255)
  @IsAlpha()
  @ApiProperty()
  fname: string;

  @IsString()
  @IsOptional()
  @Length(2, 255)
  @IsAlpha()
  @ApiProperty()
  lname: string;

  @IsString()
  @IsOptional()
  @Length(3, 15)
  @IsNumberString({
    no_symbols: true,
  })
  @ApiProperty()
  phone_number: string;
}
