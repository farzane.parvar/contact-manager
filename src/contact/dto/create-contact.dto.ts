import { ApiProperty } from '@nestjs/swagger';
import {
  IsAlpha,
  IsNotEmpty,
  IsNumberString,
  IsString,
  Length,
} from 'class-validator';

export class CreateContactDto {
  @IsString()
  @IsNotEmpty()
  @Length(2, 255)
  @IsAlpha()
  @ApiProperty()
  fname: string;

  @IsString()
  @IsNotEmpty()
  @Length(2, 255)
  @IsAlpha()
  @ApiProperty()
  lname: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 15)
  @IsNumberString({
    no_symbols: true,
  })
  @ApiProperty()
  phone_number: string;
}
