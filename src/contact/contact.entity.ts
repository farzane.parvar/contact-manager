import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('contacts')
export class Contact extends BaseEntity {
  @Column()
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  fname: string;

  @Column()
  lname: string;

  @Column()
  phone_number: string;

  @Column({
    type: 'timestamp',
  })
  created_at: Date;

  @Column({
    type: 'timestamp',
  })
  updated_at: Date;

  @Column({
    type: 'timestamp',
  })
  deleted_at: Date;
}
