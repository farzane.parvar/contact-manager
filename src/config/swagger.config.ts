import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ContactModule } from 'src/contact/contact.module';

export default (app) => {
  const options = new DocumentBuilder()
    .setTitle('Contact manager api')
    .setVersion('1.0')
    .build();

  const apppDocument = SwaggerModule.createDocument(app, options, {
    include: [ContactModule],
  });

  SwaggerModule.setup('api/docs', app, apppDocument);
};
