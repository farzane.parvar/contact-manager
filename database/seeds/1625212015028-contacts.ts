import { Contact } from '../../src/contact/contact.entity';
import { getRepository, MigrationInterface } from 'typeorm';
import * as faker from 'faker';
import { CreateContactDto } from 'src/contact/dto/create-contact.dto';

export class contacts1625212015028 implements MigrationInterface {
  public async up(): Promise<void> {
    const fakeRowCount = 10;
    const contacts: Array<CreateContactDto> = [];

    // Generate some fake contacts
    for (let i = 0; i < fakeRowCount; i++) {
      const fakeNumber = faker.phone.phoneNumberFormat(2);

      contacts.push({
        fname: faker.name.firstName(),
        lname: faker.name.lastName(),
        phone_number: fakeNumber.split('-').join(''), // Remove all - symbols to make it contain only digits
      });
    }

    // Insert them to database
    await getRepository(Contact).save(contacts);
  }

  public async down(): Promise<void> {
    // Do nothing
  }
}
