import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import postgresConnectionOptionsOptions from '../../src/config/typeorm.config';

/**
 * Typeorm doesn't support seed by default
 * Another typeorm config option is created to be used by only seed command
 */
const seedConnectionOption: PostgresConnectionOptions = {
  ...postgresConnectionOptionsOptions,
  migrations: ['database/seeds/*{.ts,.js}'],
  cli: {
    migrationsDir: 'database/seeds',
  },
};

export default seedConnectionOption;
