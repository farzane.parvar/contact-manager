<p align="center">Contact manager Rest API</p>

## Installation

1. Install Node with npm

2. Install Postgres

3. Create a postgres database

4. Copy .env.example to .env

5. Fill .env file with your server properties

6. Install npm dependencies

   ```bash
   $ npm install
   ```

7. Run migrations

   ```bash
   $ npm run migrate
   ```

8. Run seeds

   ```bash
   $ npm run seed
   ```

## Running the app

```bash
$ npm run start:dev
```

## API document

Document is created via Swagger. You can access to the document with the following URL:

`http://${host}:${port}/api/docs`

**Do not forget to replace ${host}
and ${port} with your server information**
